package model;

public class User {
    private String password;
    private String userName;
    private boolean Enabled;

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public boolean isEnabled() {
        return Enabled;
    }

    public void setEnabled(boolean enabled) {
        Enabled = enabled;
    }

    public User() {
    }

    public User(String password, String userName, boolean enabled) {
        this.password = password;
        this.userName = userName;
        this.Enabled = enabled;
    }

    @Override
    public String toString() {
        return "passworf " + password + " ,userName: " + userName + " enabled: " + Enabled;
    }

}
